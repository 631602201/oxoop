/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wariya.oxoop;

import java.util.Scanner;

/**
 *
 * @author wariy
 */
public class Game {
    private Player O;
    private Player X;
    private int row;
    private int col;
    private Board board;
    Scanner kb = new Scanner(System.in);
    
    public Game(){
        this.O = new Player('O');
        this.X = new Player('X');
    }
    
    
    
    public void newBoard(){
        this.board = new Board(O, X);
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    public void showTurn(){
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    
    public void showTable() {
        char[][] table = this.board.getTable();
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }
    
     public void inputRowCol(){
        while(true){
            System.out.print("Please input row, col:");
            row = kb.nextInt();
            col = kb.nextInt();
            if(board.setRowCol(row, col)){
                return;
            }
        }
    }
     
     public boolean isFinish(){
        if(board.isDraw() || board.isWin()){
            return true;
        }
        return false;
    }
     
     public void showStat(){
        System.out.println(O);
        System.out.println(X);
    }

     
     public void showResult(){
        if(board.isDraw()){
            System.out.println("Draw!!! ");
        }
        else if(board.isWin()){
            System.out.println(board.getCurrentPlayer().getSymbol()+" Win");
        }
    }




}
